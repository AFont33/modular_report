# daily_report
This package allows the automatic generation of PDF reports (summaries) of BPOD
animal training sessions. The plots can be easily added and removed and are
programmed as a normal python file.

## Installation and use
1. Clone the package into a suitable location:

    ```git clone https://AFont33@bitbucket.org/AFont33/modular_report.git && cd modular_report```
 
2. Install it (remember to activate the pybpod-environment first):
 
    ```pip install -e .```
 
3. This package has a configuration file inside
 modular_reports/report_settings.py that contains the paths for the PDF reports
 and for the report scripts. The default location is $HOME/projects, which
 should match PyBpod's project folder. Edit it if that's not the case or if you
 prefer another location.
 
 4. Put the reports in the folder specified in step 3.

5. For PybPod to make your reports automatically after a session, put the following lines in the task:

    ```from modular_report.main import main  # with the rest of the imports```

    ```main(glob.glob('*.csv')[0])  # last line of the task```

    Make sure that you have also imported glob with `import glob`.

6. To make the reports manually, go to the directory that contains the CSV file
   and call `make-report` from the terminal. This option scans the directory
   recursively and makes the report of all the CSV files that it finds.
   
7. There's a second option for the manual generation of reports. There's a special folder inside the package's directory called `csvs/`. From the script folder, calling main:

    ```python main.py```

    will scan the `csvs/` folder and produce the reports. This approach allows two
    terminal arguments:

    * `--log` will output the log into the terminal instead of into the designated log file.
    * `--test` will run the script in an infinite loop. This is useful to test new report scripts. Ctrl-C to exit.

