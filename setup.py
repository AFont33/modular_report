#!/usr/bin/python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name='modular_report',
    version='3.0',
    description="A package for the automatic production of BPOD session reports.",
    author='Albert Font',
    author_email='albertf80@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://AFont33@bitbucket.org/AFont33/modular_report.git',
    include_package_data=True,
    packages=['modular_report'],
    entry_points = {
        'console_scripts': ['make-report=modular_report.make_report:make_report'],
    }
)
