import os

# Path and name for the folder that contains the report scripts:
PLOTS_DIR = os.path.expanduser(os.path.join('~', 'projects', 'reports'))
HIDDEN_FILE_NAME = 'session_data.json'
# Folder that will contain t he actual reports:
HIDDEN_FILE_PATH = os.path.expanduser(os.path.join('~', 'daily_reports'))
# Folder for the manual report generation:
PATH_TO_CSV = 'csvs'

LOG_PATH = os.path.expanduser(os.path.join('~', 'daily_reports'))
LOG_FILENAME = 'report_logs.log'

# Make the reports folders if they do not exist:
for path in (PLOTS_DIR, HIDDEN_FILE_PATH):
    if not os.path.exists(path):
        os.makedirs(path)
