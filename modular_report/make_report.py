from modular_report.main import main
import os

def make_report():
    # Manual report generation.
    file_list = []
    for root, _, files in os.walk(os.getcwd()):
        for file in files:
            if file.endswith('.csv'):
                file_list.append(os.path.join(root, file))
    assert file_list, "I couldn't find the CSV files. Exiting ..."
    main(file_list)
