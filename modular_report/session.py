import logging
from collections import namedtuple

import numpy as np
import pandas as pd
from scipy import stats
from scipy.optimize import minimize
from collections import defaultdict

logger = logging.getLogger(__name__)

# ------------------------------------------------------------------------------
# Some custom exceptions:

class RewardSideNotFound(Exception):
    pass

class EmptySession(Exception):
    pass
# ------------------------------------------------------------------------------

class Session:
    """
    This class is a data holder for a single animal training session, designed to 
    parse a CSV coming from a BPOD device. It contains the following public attributes:
        · metadata (namedtuple): information about the session, like animal name, date, 
                                 stage number, etc.
        · raw_data (namedtuple): data and vectors extracted directly from the CSV file.
        · performance (namedtuple): calculations relative to the performance of the animal.
        · psych_curve (namedtuple): if applicable, contains the data relative to the
                                    psychometric curve that fits the session.
        · poke_histogram (np.array): contains data to plot a center poke time histogram.
    It also contains the following properties:
        · has_poke_histogram
        · has_psych_curve
        · has_response_time
    which return a boolean.
    """

    # Named tuples to contain the data:
    performance = namedtuple('performance', 
                            ['valids_total',
                            'valids_R', 
                            'valids_L',
                            'corrects_total',
                            'corrects_R', 
                            'corrects_L',
                            'valids_R_per_cent',
                            'valids_L_per_cent',
                            'corrects_per_cent',
                            'corrects_R_per_cent',
                            'corrects_L_per_cent',
                            'absolute_total',
                            'absolute_R', 
                            'absolute_L',
                            'ra_total', 
                            'ra_R', 
                            'ra_L',
                            'water', 
                            'invalids_total',
                            'micropokes_per_cent',
                            'invalids_per_cent'])

    metadata = namedtuple('metadata', 
                            ['box', 
                            'session_name', 
                            'subject_name', 
                            'time', 
                            'day',
                            'stage_number'])

    raw_data = namedtuple('raw_data', 
                            ['hithistory', 
                            'reward_side',
                            'response_time', 
                            'coherences',
                            'stimulus_duration',
                            'invalid_indices',
                            'valid_indices',
                            'R_indices',
                            'L_indices',
                            'R_values',
                            'L_values',    
                            'cpoke_times',    
                            'micropokes'])

    psych_curve = namedtuple('psych_curve',
                            ['xdata', 
                            'ydata', 
                            'fit', 
                            'params', 
                            'fit_error'])

    timestamps = namedtuple('timestamps',
                            ['state_list', 
                            'state_timestamps', 
                            'poke_timestamps'])


    def __init__(self, path):
        self._path = path
        self._df = None #  holds the pandas dataframe
        self._length = None 
        self.has_invalids = False
        # Four namedtuples which contain the main pieces of session information:
        self.metadata = None  
        self.raw_data = None
        self.performances = None
        self.psych_curve = None

        # Read the dataframe and populate the namedtuples:
        self._read_dataframe()
        self._compute_metadata()
        self._compute_raw_data()
        self._compute_performances()  
        if self.has_psych_curve:
            self._compute_psych_curve()
        # self._compute_timestamps()

    def __len__(self):
        """
        Returns the total number of trials (valids and invalids) of the session.
        """
        return self._length

    def _read_dataframe(self):
        """
        Take the path of a CSV file and read it as a pandas dataframe.
        """
        try:
            dataframe = pd.read_csv(self._path, skiprows=6, sep=';')
        except FileNotFoundError:
            logger.critical("Error: CSV file not found. Exiting...")
            raise
        else:
            self._df = dataframe

    def _compute_metadata(self):
        """
        This function extracts metadata from the pandas dataframe and saves it into 
        a namedtuple. 
        """

        try:
            box = self._df[self._df.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
        except IndexError:
            box = "Unknown box"
            logger.warning("Box name not found.")
            
        try:
            session_name = self._df[self._df.MSG == 'SESSION-NAME']['+INFO'].iloc[0]
        except IndexError:
            session_name = "Unknown session"
            logger.warning("Session name not found. Saving as 'Unknown session'.")

        try:
            subject_name = self._df[self._df.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
            subject_name = subject_name[1:-1].split(',')[0].strip("'")
        except IndexError:
            subject_name = "Unknown subject"
            logger.warning("Subject name not found.")
            
        try:
            session_started = self._df[self._df.MSG == 'SESSION-STARTED']['+INFO'].iloc[0]
            date = session_started.split() #  f.e. ['2018-08-13', '11:25:18.238172']
            time = date[1][0:8]
            day = date[0]
        except IndexError:
            time = "??"
            day = "??"
            logger.warning("Session start time not found.")
            
        try:
            stage_number = self._df[self._df.MSG == 'STAGE_NUMBER']['+INFO'].iloc[0]
            stage_number = int(stage_number)
        except IndexError:
            stage_number = np.nan
            logger.warning("Stage number not found.")

        self.metadata = Session.metadata(box=box, 
                                        session_name=session_name, 
                                        subject_name=subject_name,
                                        time=time, 
                                        day=day,
                                        stage_number=stage_number)

        logger.info(f"{subject_name}, ({day} - {time})")
        logger.info("Session metadata loaded.")


    def _compute_raw_data(self):
        """
        Extracts the main vectors and data from the CSV file: reward_side,
        hithistory, session length, response time, coherences and transition 
        information (for the poke histogram).
        """

        # -----------------------------------------------------------------------------
        # Extract the main states of the session. They contain a value if the trial passed
        # through that state, NaN otherwise. Here, the main states are punish,
        # invalids, and reward.

        punish_data = self._df.query("TYPE=='STATE' and MSG=='Punish'")['BPOD-FINAL-TIME'].astype(float)
        invalids = self._df.query("TYPE=='STATE' and MSG=='Invalid'")['BPOD-FINAL-TIME'].astype(float)
        reward_data = self._df.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-FINAL-TIME'].astype(float)
        
        # Set the invalids flag:
        if invalids.size:
            self.has_invalids = True

        # Since the state machines are designed so that the main states are mutually exclusive in a single trial, we 
        # can compute the total session length as the sum of all the values that are not NaN:
        self._length = (punish_data.dropna().size
                        + reward_data.dropna().size
                        + invalids.dropna().size)

        if not self._length:
            raise EmptySession("Session results not found; report can't be generated. Exiting...")

        # Detect which are the indices of those states that make a trial not be valid
        # (here, invalid):
        invalid_indices = np.where(~np.isnan(invalids))[0]
        valid_indices = np.where(np.isnan(invalids))[0]

        # Compute hithistory vector; it will now contain True if the answer 
        # was correct, False otherwise:
        hithistory = np.isnan(punish_data.values)[:self._length]

        # -----------------------------------------------------------------------------
        # REWARD_SIDE contains a 1 if the correct answer was the (R)ight side, 0 otherwise.
        # It is received as a string from the CSV: "[0,1,0,1,0,1,1,1,1,0,0,1,0,1,1,1]"...
        # and there can be more than one in the file. We always take THE LAST (iloc[-1]).
        try:
            reward_side_str = self._df[self._df.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1][1:-1].split(',')[:self._length]
        except IndexError: #  Compatibility with old files, when REWARD_SIDE was called VECTOR_CHOICE.
            logger.warning("REWARD_SIDE vector not found. Trying old VECTOR_CHOICE...")
            try:
                reward_side_str = self._df[self._df.MSG == 'VECTOR_CHOICE']['+INFO'].iloc[-1][1:-1].split(',')[:self._length]
            except IndexError:
                raise RewardSideNotFound("Neither REWARD_SIDE nor VECTOR_CHOICE found. Exiting...")
        else:
            # Cast to int from str:
            reward_side = np.array(reward_side_str, dtype=int)
        
        # Indices of left and right trials, omitting invalid trials:
        R_indices = np.where(np.delete(reward_side, invalid_indices))[0]
        L_indices = np.where(np.delete(reward_side, invalid_indices) == 0)[0]
        # Contains 1 for correct and valid trials, 0 otherwise, only for right side:
        R_values = np.take(hithistory, R_indices)
        # Same for left side:
        L_values = np.take(hithistory, L_indices)

        # -----------------------------------------------------------------------------
        # Response times for the session and their mean:
        response_time = self._df[self._df.MSG == "WaitResponse"]['+INFO'].values[:self._length].astype(float)
        
        if response_time.size:
            if self.has_invalids:
                # Don't take invalid trials into account:
                response_time = np.delete(response_time, invalid_indices)
            # Take NaNs out:
            response_time =  response_time[~np.isnan(response_time)]
            # Remove outliers higher than 1 second or negative:
            response_time = response_time[(response_time > 0) & (response_time < 1)]
            # Convert to ms and return an int:
            response_time = int(np.mean(response_time) * 1000) 
        else:
            response_time = np.nan
            logger.warning("No response time found, it is undefined from now on.")

        # -----------------------------------------------------------------------------
        # coherences vector, from 0 to 1 (later it will be converted 
        # into evidences from -1 to 1):
        coherences =  self._df[self._df['MSG'] == 'coherence01']['+INFO'].values[:self._length].astype(float)
        # Delete invalid trials:
        coherences = np.delete(coherences, invalid_indices)
        if not coherences.size:
            logger.warning("This trial doesn't use coherences.") 

        # -----------------------------------------------------------------------------
        # Stimulus duration section, not needed for now.
        """
        startSound = df.query("TYPE == 'STATE' and MSG == 'StartSound'")['+INFO'].values[:length]

        if startSound.size:
            startSound = [round(float(elem) * 1000, 1) for elem in startSound]
        else:
            startSound = []

        # Stimulus duration for the staircase plot inside the daily report:
        stimulus_duration = df.query("TYPE == 'STATE' and MSG=='KeepSoundOn'")['+INFO'].values[:length]
        if not stimulus_duration.size:
            logger.info("Stimulus duration info not found.")
            stimulus_duration = []
        else:
            stimulus_duration = parse_stimulus_duration(stimulus_duration)
            if startSound.size:
                stimulus_duration = list(map(sum, zip(stimulus_duration, startSound)))
        """
        stimulus_duration = []    

        # -----------------------------------------------------------------------------
        # Fixation times for the cpoke histogram and the total number of micropokes:
        fixation_times = self._df.query("TYPE=='STATE' and MSG=='Fixation'")['+INFO'].dropna().values.astype(float)
        micropokes = len(fixation_times[fixation_times < 0.299])
        cpokes = self._df.query("TYPE=='STATE' and MSG=='StartSound'")['+INFO'].dropna().values.astype(float) + 0.3
        cpoke_times = np.concatenate((fixation_times[fixation_times < 0.299], cpokes), axis=None) * 1000
        # -----------------------------------------------------------------------------
        # Now that we have calculated all the indices, we can remove the invalids
        # from reward_side and hithistory:
        hithistory = np.delete(hithistory, invalid_indices)
        reward_side = np.delete(reward_side, invalid_indices)

        self.raw_data = Session.raw_data(hithistory=hithistory, 
                                        reward_side=reward_side, 
                                        response_time=response_time, 
                                        coherences=coherences, 
                                        stimulus_duration=stimulus_duration, 
                                        invalid_indices=invalid_indices,
                                        valid_indices=valid_indices,
                                        R_indices=R_indices,
                                        L_indices=L_indices,
                                        R_values=R_values,
                                        L_values=L_values,
                                        cpoke_times=cpoke_times,
                                        micropokes=micropokes)

        logger.info("Session raw data loaded.")
    
    def _compute_performances(self):
        """ 
        Computes information relative to performances. We have three main categories:
        total performance, left performance, and right performance. For each of these, 
        we need two pieces of information: the absolute performance (as % of valid trials) 
        and the windowed (20 samples) performance. We also compute here useful percentages.
        """

        def compute_window(data):
            """ 
            Computes a rolling average with a length of 20 samples.
            """
            performance = []
            for i in range(len(data)):
                if i < 20: 
                    performance.append(round(np.mean(data[0:i+1]), 2))
                else:
                    performance.append(round(np.mean(data[i-20:i]), 2))

            return performance
        
        percentage = lambda part, total: np.around(part * 100 / total, 1)
        
        # For the performance display, take into account that invalid trials do not count towards
        # overall performance; hence, the indices of the valid trials must be tracked for the displays
        # and calculations.

        if self.has_invalids:
            # Total number of invalid trials:
            invalids_total = len(self.raw_data.invalid_indices)
            # Total number of valid trials:
            valids_total = len(self) - invalids_total
        else:
            invalids_total = np.nan
            valids_total = len(self)
        valids_R = len(self.raw_data.R_values)
        valids_L = valids_total - valids_R
        # Total number of correct trials:
        corrects_total = np.count_nonzero(self.raw_data.hithistory)
        corrects_R = np.count_nonzero(self.raw_data.R_values)
        corrects_L = corrects_total - corrects_R
        # Percentage calculation. Invalids % is relative to total trials; valids on R and L and total
        # corrects are relative to total valids; corrects on R and L are relative to valids on R and L.
        invalids_per_cent = percentage(invalids_total, len(self))
        valids_R_per_cent = percentage(valids_R, valids_total)
        valids_L_per_cent = percentage(valids_L, valids_total)
        corrects_per_cent = percentage(corrects_total, valids_total) # AKA, accuracy
        corrects_R_per_cent = percentage(corrects_R, valids_R)
        corrects_L_per_cent = percentage(corrects_L, valids_L)
        micropokes_per_cent = percentage(self.raw_data.micropokes, valids_total)
        # Total (absolute) performances:
        absolute_total = np.around(np.mean(self.raw_data.hithistory), 2)
        absolute_R = np.around(np.mean(self.raw_data.R_values), 2)
        absolute_L = np.around(np.mean(self.raw_data.L_values), 2)
        # Rolling averages:
        ra_total = compute_window(self.raw_data.hithistory)
        ra_R = compute_window(self.raw_data.R_values)
        ra_L = compute_window(self.raw_data.L_values)

        # Each correct trial implies 24 uL of water; we display it in mL in the report:
        water = np.around(corrects_total * 0.024, 3)
        
        # Save the data in the biggest namedtuple you've ever seen:
        self.performance = Session.performance(valids_total=valids_total,
                                               valids_R=valids_R, 
                                               valids_L=valids_L,
                                               corrects_total=corrects_total,
                                               corrects_R=corrects_R, 
                                               corrects_L=corrects_L,
                                               valids_R_per_cent=valids_R_per_cent,
                                               valids_L_per_cent=valids_L_per_cent,
                                               corrects_per_cent=corrects_per_cent,
                                               corrects_R_per_cent=corrects_R_per_cent,
                                               corrects_L_per_cent=corrects_L_per_cent,
                                               absolute_total=absolute_total,
                                               absolute_R=absolute_R, 
                                               absolute_L=absolute_L,
                                               ra_total=ra_total, 
                                               ra_R=ra_R, 
                                               ra_L=ra_L,
                                               water=water, 
                                               invalids_total=invalids_total,
                                               micropokes_per_cent=micropokes_per_cent,
                                               invalids_per_cent=invalids_per_cent)

        logger.info("Session performances loaded.")

    def _compute_psych_curve(self):
        """
        If the session has coherences, this method will fit a psychometric curve.
        """

        def sigmoid_MME(fit_params: tuple):
            """
            This function is used by the minimizer to compute the fit parameters.
            """
            k, x0, B, P = fit_params
            # Function to fit:
            yPred = B + (1 - B - P)/(1 + np.exp(-k * (xdata - x0)))
            # Calculate negative log likelihood:
            LL = - np.sum(stats.norm.logpdf(ydata, loc=yPred))

            return LL
        
        evidences = self.raw_data.coherences * 2 - 1
        # R_resp contains True if the response was on the right side, whether correct or incorrect.
        R_resp = np.logical_not(np.logical_xor(self.raw_data.reward_side, self.raw_data.hithistory))
        a = {'R_resp': R_resp, 'evidence': evidences, 'coh': self.raw_data.coherences}
        coherence_dataframe = pd.DataFrame(a)

        info = coherence_dataframe.groupby(['evidence'])['R_resp'].mean()
        ydata = [np.around(elem, 3) for elem in info.values]
        xdata = info.index.values
        fit_error = [np.around(elem, 3) for elem in coherence_dataframe.groupby(['coh'])['R_resp'].sem().values]

        # Run the minimizer:
        LL = minimize(sigmoid_MME, [1, 1, 0, 0])
        
        # Fit parameters:
        k, x0, B, P = [np.around(param, 2) for param in LL['x']]
        # Compute the fit with 30 points:
        fit = B + (1 - B - P)/(1 + np.exp(-k * (np.linspace(-1, 1, 30) - x0)))
        fit = [np.around(elem, 3) for elem in fit]

        self.psych_curve = Session.psych_curve(xdata=xdata, 
                                            ydata=ydata,
                                            fit=fit, 
                                            params=[k, x0, B, P],
                                            fit_error=fit_error)

        logger.info("Session psychometric curve done loaded.")

    def _compute_timestamps(self):
        """
        Extracts information about the session's state and poke timestamps. Works with any session 
        type. state_list contains a list of all the unique states; state_timestamps contains the 
        timestamps for each state, both _start and _end.
        """
        # --------------- Computation of the states list ----------
        new_trial = self._df.query("TYPE=='TRIAL' and MSG=='New trial'").index[1]
        last_trial = self._df.query("TYPE=='END-TRIAL'").index[0]
        trial_band_states = self._df.query("TYPE=='STATE' and index > @last_trial and index < @new_trial")
        state_list = set(trial_band_states['MSG'].values)

        states = defaultdict(list)
        pokes = defaultdict(list)

        new_trial_indexes = self._df.query("TYPE=='TRIAL' and MSG=='New trial'").index[:self._length]
        for jj in range(self._length):

            if jj != self._length - 1:
                index_1, index_2 = new_trial_indexes[jj], new_trial_indexes[jj+1]
                trial_band_events = self._df.query("TYPE=='EVENT' and index > @index_1 and index < @index_2")
                trial_band_states = self._df.query("TYPE=='STATE' and index > @index_1 and index < @index_2")
            else:
                index_1 = new_trial_indexes[jj]
                trial_band_events = self._df.query("TYPE=='EVENT' and index > @index_1")
                trial_band_states = self._df.query("TYPE=='STATE' and index > @index_1")

        # -------------------------- STATE TIMESTAMPS ----------------------------------- #
            for state in state_list:
                start_times = trial_band_states.query("MSG==@state")['BPOD-INITIAL-TIME'].values.tolist() 
                end_times = trial_band_states.query("MSG==@state")['BPOD-FINAL-TIME'].values.tolist()                 
                states[f"{state}_start"].append(start_times)
                states[f"{state}_end"].append(end_times)

        # --------------------------- POKE TIMESTAMPS ----------------------------------- #
            found_L_in = found_C_in = found_R_in = found_L_out = found_C_out = found_R_out = False
            C_start = []
            L_start = []
            R_start = []
            C_end = []
            L_end = []
            R_end = []
            for (_, row) in trial_band_events.iterrows():
                if row['+INFO'] == 'Port1In': # Port1In
                    found_L_in = True
                    if found_L_out:
                        L_start.append(np.nan)
                        found_L_out = False
                    L_start.append(row['BPOD-INITIAL-TIME'])
                elif row['+INFO'] == 'Port2In': # Port2In
                    found_C_in = True
                    if found_C_out:
                        C_start.append(np.nan)
                        found_C_out = False
                    C_start.append(row['BPOD-INITIAL-TIME'])
                elif row['+INFO'] == 'Port3In': # Port3In
                    found_R_in = True
                    if found_R_out:
                        R_start.append(np.nan)
                        found_R_out = False
                    R_start.append(row['BPOD-INITIAL-TIME'])
                elif row['+INFO'] == 'Port1Out': # Port1Out
                    if not found_L_in:
                        found_L_out = True
                    else:
                        found_L_in = False
                    L_end.append(row['BPOD-INITIAL-TIME'])
                elif row['+INFO'] == 'Port2Out': # Port2Out
                    if not found_C_in:
                        found_C_out = True
                    else:
                        found_C_in = False
                    C_end.append(row['BPOD-INITIAL-TIME'])
                elif row['+INFO'] == 'Port3Out': # Port3Out
                    if not found_R_in:
                        found_R_out = True
                    else:
                        found_R_in = False
                    R_end.append(row['BPOD-INITIAL-TIME'])
                else:
                    pass

            if found_L_in:
                L_end.append(np.nan)
            if found_C_in:
                C_end.append(np.nan)
            if found_R_in:
                R_end.append(np.nan)

            if found_L_out:
                L_start.append(np.nan)
            if found_C_out:
                C_start.append(np.nan)
            if found_R_out:
                R_start.append(np.nan)   
                

            pokes['L_e'].append(L_end)
            pokes['C_e'].append(C_end)
            pokes['R_e'].append(R_end)
            pokes['R_s'].append(R_start)
            pokes['C_s'].append(C_start)
            pokes['L_s'].append(L_start)

        self.timestamps = Session.timestamps(state_list=state_list,
                                            state_timestamps=states,
                                            poke_timestamps=pokes)

    @property
    def has_psych_curve(self) -> bool:
        return bool(self.raw_data.coherences.size)

    @property
    def has_cpoke_histogram(self) -> bool:
        return bool(self.raw_data.cpoke_times.size)
    
    @property
    def has_response_time(self) -> bool:
        return bool(self.raw_data.response_time)
